﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Discord.Commands;

namespace Omnis.Rex
{
    public class Logging
    {
        private static readonly string MainLogFile = "OmnisLog";

        public static readonly object LogFileLock = new object();
        private static readonly string ConsolePrefix = "+ ";
        private static readonly string LogPath = Path.Combine(AppContext.BaseDirectory, "logs");
        private static readonly string OmnisLogPath = Path.Combine(LogPath, MainLogFile);

        public Logging()
        {
            if (!Directory.Exists(LogPath))
                Directory.CreateDirectory(LogPath);
            if (!File.Exists(OmnisLogPath))
                File.Create(OmnisLogPath);
        }

        private static string GetLogTime()
        {
            int hour = DateTime.Now.TimeOfDay.Hours;
            int minute = DateTime.Now.TimeOfDay.Minutes;
            int second = DateTime.Now.TimeOfDay.Seconds;
            string ampm = hour <= 11 ? "AM" : "PM";
            string logHour = hour == 0 ? "12" : (hour <= 12 ? hour : 24 - (hour + 12)).ToString();
            string logMin = minute < 10 ? "0" + minute : minute.ToString();

            return $"[{logHour}:{logMin} {ampm}] |";
        }

        private static void AppendPrefix()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write(ConsolePrefix);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write(GetLogTime());
        }

        /// <summary>
        /// Logs a message to a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message"></param>
        public void LogToFile(string filename, string message)
        {
            lock (LogFileLock)
            {
                File.AppendAllText($"{LogPath}/{filename}", $"{DateTime.Now} | {message}\n");
            }
        }

        /// <summary>
        /// Sends a normal log message.
        /// </summary>
        /// <param name="message"></param>
        public void Normal(string message)
        {
            lock (this)
            {
                AppendPrefix();
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(message);
                this.LogToFile(MainLogFile, $"[GENERIC] >>> {message}");
            }
        }

        /// <summary>
        /// Sends a positive log message.
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="idcolor"></param>
        /// <param name="message"></param>
        public void Positive(string identity, ConsoleColor idcolor, string message)
        {
            lock (this)
            {
                AppendPrefix();
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("[");
                Console.ForegroundColor = idcolor;
                Console.Write(identity);
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("] => ");
                Console.WriteLine(message);
                this.LogToFile(MainLogFile, $"[{identity.ToUpper()}] >>> {message}");
            }
        }

        /// <summary>
        /// Sends a negative log message. Color = Red
        /// </summary>
        /// <param name="message"></param>
        public void Negative(string message)
        {
            lock (this)
            {
                AppendPrefix();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"[DANGER] => {message}");
                this.LogToFile(MainLogFile, $"[DANGER] => {message}");
            }
        }

        /// <summary>
        /// Sends the log of an exception. Color = Red
        /// </summary>
        /// <param name="ex"></param>
        public void Negative(Exception ex)
        {
            lock (this)
            {
                AppendPrefix();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"[DANGER] => {ex}");
                this.LogToFile(MainLogFile, $"[DANGER] => {ex}");
            }
        }

        /// <summary>
        /// Sends a warning log message. Color = Yellow
        /// </summary>
        /// <param name="message"></param>
        public void Warning(string message)
        {
            lock (this)
            {
                AppendPrefix();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"[WARNING] => {message}");
                this.LogToFile(MainLogFile, $"[WARNING] = > {message}");
            }
        }

        /// <summary>
        /// Sends an error log message. Color = Dark Red
        /// </summary>
        /// <param name="message"></param>
        public void Error(string message)
        {
            lock (this)
            {
                AppendPrefix();
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("█ ! █ ERROR █ ! █");
                Console.WriteLine(message);
                this.LogToFile(MainLogFile, $"[ERROR] => {message}");
            }
        }

        /// <summary>
        /// Sends a good log message. Color = Green
        /// </summary>
        /// <param name="message"></param>
        public void Good(string message)
        {
            lock (this)
            {
                AppendPrefix();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"[GOOD] => {message}");
                this.LogToFile(MainLogFile, $"[GOOD] => {message}");
            }
        }

        public void Notify(string message)
        {
            Console.WriteLine($"\n\t------------\\\\\\\\\\ {message} //////////------------\n");
        }
    }
}
