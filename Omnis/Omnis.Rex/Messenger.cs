﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace Omnis.Rex
{
    public class Messenger
    {
        public Logging Log { get; }
        public Messenger(Logging logging)
        {
            Log = logging;
        }

        private void LogFailedToMessage(IChannel channel, Exception ex)
        {
            string log = string.Empty;
            if (channel is IGuildChannel guildChannel)
            {
                log += $"({guildChannel.Guild.Name} - {guildChannel.Name}): {ex.Message}";
            }
            else
            {
                IDMChannel dmChannel = (IDMChannel)channel;
                log += $"(Direct Message) {dmChannel.Recipient}: {ex.Message}";
            }
            Log.Positive("Messenger", ConsoleColor.Red, log);
        }

        private bool CanSendMessage(IChannel channel)
        {
            if (channel == null)
                return false;

            if (channel is SocketGuildChannel guildChannel)
            {
                SocketGuildUser botGuildUser = guildChannel.Guild.CurrentUser;
                return botGuildUser.GetPermissions(guildChannel).SendMessages;
            }

            return false;
        }

        /// <summary>
        /// Starts Discord's "is typing..." event
        /// </summary>
        /// <param name="channel"></param>
        /// <returns></returns>
        public async Task StartTypingAsync(IChannel channel)
        {
            try
            {
                IMessageChannel messageChannel = channel as IMessageChannel;

                if (!CanSendMessage(messageChannel))
                    return;

                await messageChannel.TriggerTypingAsync();
            }
            catch (Exception ex) // Figure out what to catch here
            {
                LogFailedToMessage(channel, ex);
            }
        }

        public async Task<IUserMessage> SendMessageAsync(IChannel channel, string message, IMessage context = null)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(message))
                    return null;

                IMessageChannel msgChannel = channel as IMessageChannel;

                if (!CanSendMessage(msgChannel))
                    return null;

                return await msgChannel.SendMessageAsync(message);
            }
            catch (Exception ex)
            {
                LogFailedToMessage(channel, ex);
            }

            return null;
        }

        public async Task<IUserMessage> SendMessageAsync(IChannel channel, EmbedBuilder eb)
        {
            try
            {
                if (eb == null)
                    return null;

                IMessageChannel msgChannel = channel as IMessageChannel;

                if (!CanSendMessage(msgChannel))
                    return null;

                return await msgChannel.SendMessageAsync(string.Empty, false, eb.Build());
            }
            catch (Exception ex)
            {
                LogFailedToMessage(channel, ex);
            }

            return null;
        }

        public async Task<IUserMessage> SendMessageAsync(IMessage message, EmbedBuilder eb)
        {
            return await SendMessageAsync(message.Channel, eb);
        }
    }
}
