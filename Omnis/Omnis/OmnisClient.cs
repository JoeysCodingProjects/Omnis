﻿using System;
using System.Collections.Generic;
using System.Text;
using Discord.Rest;
using Discord.WebSocket;
using DiscordBotsList.Api;
using Omnis.Rex;

namespace Omnis
{
    public class OmnisClient
    {
#if DEBUG
        private readonly bool DevMode = true;
#else
        private readonly bool DevMode = false;
#endif
        private readonly string DiscordToken;

        public string Prefix { get; }
        public DiscordShardedClient DiscShardClient { get; }
        public DiscordRestClient DiscRestClient { get; }
        public Logging Log { get; }

        public readonly AuthDiscordBotListApi DiscordBotList;

        public OmnisClient(string token, string prefix,)
        {
            Console.Clear();
            Console.Title = DevMode ? "Omnes Videntes" : "[DEBUG] Omnes Videntes [DEBUG]";

            this.DiscordToken = token;
            this.Prefix = prefix;
        }
    }
}
