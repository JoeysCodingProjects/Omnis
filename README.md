# Omnis, The "Jack of All Trades" Discord Bot

## Descrition
Omnis is a multi-purpose Discord bot, created to be capable of existing on any number of servers. It (will) offer tons of utility, moderation, and miscellaneous commands with the hopes of being suitable for any server. For a list of planned features, see the **Features** section.

Omnis, meaning "all" in latin, will be written in C# using the Discord.Net wrapper. I will list all the dependencies at the bottom of the readme, adding new ones as I go.

## Features
- [ ] Basic utility/moderation (automatic role assignment, customizable language filter, warnings/timeouts, polls/voting etc.)
- [ ] Harder to impliment utilities (possibly clearing a channel or all of a user's recent messages, spam/phishing account protection)
- [ ] Audit log monitering to keep moderators up to date on all/mild/extreme changes to the server by posting them into a specified channel
- [ ] Full music bot functionality
- [ ] Twitch.Tv live stream notifications (may be discarded due to rate limits with twitch api)
- [ ] Command for automatic permissions creation for categorys (will not do for individual channels as adjusting individually is poor practice)
- [ ] Video Game related information (Will only be done for a few popular, competitive games like LoL, Valorant, Apex, etc.) like patch notes, player stats, etc.
- [ ] Some kind of **optional** leveling system with maybe some loot n stuff idk we will see
- [ ] And certainly much more.

**If you'd like to suggest a feature, please do so through issues using the "feature request" label**
